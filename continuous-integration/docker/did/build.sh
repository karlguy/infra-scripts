#!/bin/sh

set -e

echo "Setting up DID environment"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

# docker build does not allow to add the SYS_PTRACE cap
export PALUDIS_DO_NOTHING_SANDBOXY=1

echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf
cave resolve repository/virtualization repository/net -x
echo 'dev-lang/go bootstrap' >> /etc/paludis/options.conf
cave resolve dev-lang/go -xz1
sed -e 's:dev-lang/go bootstrap::g' -i /etc/paludis/options.conf
cave resolve docker --permit-old-version sys-apps/tini -x
sed '/\*\/\* build_options: -recommended_tests/d' -i /etc/paludis/options.conf

echo "Cleaning up again"
rm -f /build.sh
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

