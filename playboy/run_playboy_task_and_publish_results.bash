#!/usr/bin/env bash

export PALUDIS_NO_WRITE_CACHE_CLEAN=yes

RECIPIENTS="exherbo-job-logs@exherbo.org"
PLAYBOY_DIR="${HOME}/infra-scripts.git/playboy"
LOG="${HOME}/nightly-out"
export PALUDIS_LOG_DIRECTORY="${HOME}/paludis-logs"
export PALUDIS_HOOKS_TMPDIR="${HOME}/paludis-hooks-tmp"

die() {
    local ret line
    ret=${1}
    shift
    for line in "$@"; do
        echo "${line}"
    done >&2
    exit ${ret}
}

# exherbo,gentoo?
if [[ -n ${1} ]]; then
    TASK="${1}"
else
    die 1 "No task given."
fi
if [[ -n ${2} ]]; then
    ALWAYS_SENDMAIL=yesplease
fi

run() {
    local my_dir="${PLAYBOY_DIR}/${TASK}"
    local lockfile="${my_dir}/lockfile"
    {
        if ! flock --exclusive --nonblock 5; then
            echo -e "\nFailed to lock ${lockfile}, is another instance already running?\n"
            return 1
        fi

        local output_dir="${my_dir}/output"
        rm -f "${output_dir}"/*
        local TARBALL PLAYBOY_OPTIONS
        source "${my_dir}/task_settings.bash"

        ruby "${PLAYBOY_DIR}/playboy.rb" --environment ${ENVIRONMENT} --log-level warning \
            "${PLAYBOY_OPTIONS[@]}" --output-dir "${output_dir}" &&
        ruby "${PLAYBOY_DIR}/cleanup.rb" --environment ${ENVIRONMENT} --log-level warning &&
        (
            cd "${output_dir}" &&
            echo -e "\nTarring up things: tar cvjf ../${TARBALL} *.repository"
            tar cvjf ../${TARBALL} *.repository
            cp ../${TARBALL} /srv/www/git.exherbo.org/cgit/${TARBALL}_TMP &&
            mv /srv/www/git.exherbo.org/cgit/${TARBALL}{_TMP,}
        ) &&
        echo -e "\nGeneration complete.\n" || return 1
    } 5>"${lockfile}"
}

mail() {
    local subject="${1}" body="${2}"
    HOME="/tmp" LANG="en_GB.utf8" EMAIL="Playboy script <playboy@git.exherbo.org>" \
        mutt -s "${subject}" -- ${RECIPIENTS} < "${body}"
}

main() {
    local ENVIRONMENT date0 date1 run_date result
    date0=$(date -u +%_D) || die 1 "date0 failed"
    date1=$(date -u +%T) || die 1 "date1 failed"

    run_date=$(date -u +%Y-%m-%d_%H:%M)
    run &> "${LOG}/${TASK}-${run_date}.log" && result=Success || result=Failure

    # Remove crap from the log file.
    sed -e '/e.cache.stale/d' -i "${LOG}/${TASK}-${run_date}.log" \
        || echo -e '\nRemoving stale cache warnings failed' >> "${LOG}/${TASK}-${run_date}.log"

    if [[ ${result} == Failure && -n ${ENVIRONMENT} ]]; then
        {
            echo -e "\n# cave --environment ${ENVIRONMENT} info"
            cave --environment ${ENVIRONMENT} info 2>&1
        } >> "${LOG}/${TASK}-${run_date}.log"
    fi

    if [[ ${result} == Failure || -n ${ALWAYS_SENDMAIL} ]]; then
        mail "${result}: Playboy run for ${TASK} at ${run_date}" "${LOG}/${TASK}-${run_date}.log"
    fi
}

main

# vim: set sw=4 ts=4 tw=100 et :
